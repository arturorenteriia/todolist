<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>To Do List</title>
        <script src="/js/app.js"></script>
        <link rel="stylesheet" href="/css/app.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    </head>
    <body>
        <div id="app" class="flex-center position-ref full-height mt-3">
           <todolist></todolist>
        </div>
    </body>
</html>

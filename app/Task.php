<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Status;
class Task extends Model
{
    public function statuses()
    {
        return $this->belongsTo(Status::class);
    }
    public static function saveTask($task)
    {
        $newTask = new Task();
        $newTask->name = $task;
        $newTask->save();
        $newStatus = new Status();
        $newStatus->task_id = $newTask->id;
        $newStatus->completed = false;
        $newStatus->save();
        return $newTask;
    }

    public static function updateTask($task, $id)
    {
        $updatedTask = Task::findOrFail($id);
        $updatedTask->name = $task;
        $updatedTask->save();
        return true;
    }
}

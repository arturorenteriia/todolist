<?php

namespace App\Http\Controllers;

use App\Status;
use Illuminate\Http\Request;
use App\Task;
use Illuminate\Support\Facades\DB;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = DB::table('tasks')
        ->join('statuses', 'tasks.id', '=', 'statuses.task_id')
        ->select('tasks.id','name as task', 'statuses.completed as completed')->get();
        return $tasks;
    }

    public function changeStatus(Request $request)
    {
        $status = $request->all()['status'];
        $id = $request->all()['id'];
        $task = Status::where('task_id', $id)->first();        
        $task->completed = $status;
        $task->save();
        return true;
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Task::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task = Task::saveTask($request->all()['task']);
       
        return $this->successResponse([$task->id], 'Tasks added correctly');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = Task::updateTask($request->all()['task'], $id);
        return $this->successResponse([], 'Tasks updated correctly');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::findOrFail($id);
        $status = Status::where('task_id',$id);
        $status->delete();
        $task->delete();
        return $this->successResponse([], 'Task deleted correctly');

    }
}
